# app.py (Flask application)
from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/get', methods=['GET'])
def get_example():
    return jsonify(message="GET request received")

@app.route('/post', methods=['POST'])
def post_example():
    data = request.json  # Assuming JSON data in the request
    return jsonify(message="POST request received", data=data)

@app.route("/test", methods=["GET", "POST"])
async def route1(request):
    print("request data: ", request.json)
    return response.json(dict(status=True))

if __name__ == '__main__':
    app.run(debug=True)
